//
//  Button.swift
//  NoteWorthy
//
//  Created by Marc Michel on 9/30/18.
//  Copyright © 2018 Marc Michel. All rights reserved.
//

import UIKit

@IBDesignable
class Button: UIButton {

    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            clipsToBounds = true
        }
    }
}
