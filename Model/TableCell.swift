//
//  TableCell.swift
//  NoteWorthy
//
//  Created by Marc Michel on 10/1/18.
//  Copyright © 2018 Marc Michel. All rights reserved.
//

import UIKit

class TableCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var locked: UILabel!
    
    func updateCell(note: Note) {
        if note.lockStatus == .locked {
            title.text = "content hidden"
            message.text = "message is locked, unlock to read"
            locked.isHidden = false
        } else {
            locked.isHidden = true
            title.text = note.title
            message.text = note.message
        }
    }

}
