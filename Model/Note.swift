//
//  File.swift
//  NoteWorthy
//
//  Created by Marc Michel on 10/2/18.
//  Copyright © 2018 Marc Michel. All rights reserved.
//

import Foundation

class Note {
    
    public private(set) var title: String?
    public private(set) var message: String?
    public var lockStatus: LockStatus
    
    init(title: String, message: String, lockStatus: LockStatus) {
        self.title = title
        self.message = message
        self.lockStatus = lockStatus
    }
}
