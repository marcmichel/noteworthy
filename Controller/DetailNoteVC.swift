//
//  DetailNoteVC.swift
//  NoteWorthy
//
//  Created by Marc Michel on 10/2/18.
//  Copyright © 2018 Marc Michel. All rights reserved.
//

import UIKit

class DetailNoteVC: UIViewController {

    @IBOutlet weak var noteTitle: UILabel!
    @IBOutlet weak var messageBody: UITextView!
    
    var note: Note!
    var index: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        noteTitle.text = note.title
        messageBody.text = note.message


    }
    
    
    @IBAction func lockBtn(_ sender: Any) {
        noteArray[index].lockStatus = lockStatusFlipper(note.lockStatus)
        dismiss(animated: true, completion: nil)

    }
    
}
