//
//  ViewController.swift
//  NoteWorthy
//
//  Created by Marc Michel on 9/30/18.
//  Copyright © 2018 Marc Michel. All rights reserved.
//

import UIKit
import LocalAuthentication


class NoteVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
     
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
}

extension NoteVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return noteArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as? TableCell else { return UITableViewCell() }
        cell.updateCell(note: noteArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if noteArray[indexPath.row].lockStatus == .locked {
            authBiometric { (success) in
                if success {
                    noteArray[indexPath.row].lockStatus = lockStatusFlipper(noteArray[indexPath.row].lockStatus)
                    DispatchQueue.main.async {
                        self.pushNote(indexPath: indexPath)
                    }
                }
            }
        } else {
            self.pushNote(indexPath: indexPath)
        }
    }
    
    func pushNote(indexPath: IndexPath) {
        guard let noteDetail = self.storyboard?.instantiateViewController(withIdentifier: "DetailNoteVC") as? DetailNoteVC else { return }
        noteDetail.note = noteArray[indexPath.row]
        noteDetail.index = indexPath.row
        self.present(noteDetail, animated: true, completion: nil)
    }
    
    
    //touchID & faceId function
    
    func authBiometric(completion: @escaping(Bool) -> Void) {
        let myContext = LAContext()
        let myLocalizedReasonString = "We need touchId and faceId to secure your note"
        var authError : NSError?
        if #available(iOS 8.0, iOSMac 10.12.1, *) {
            if myContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
                myContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: myLocalizedReasonString) { (success, error) in
                    if success {
                        completion(true)
                    } else {
                        guard let evaluateErrorString = error?.localizedDescription else {return}
                        self.showAlert(withMessage: evaluateErrorString)
                        completion(false)
                    }
                }
            } else {
                guard let authErrorSting = authError?.localizedDescription else {return}
                self.showAlert(withMessage: authErrorSting)
                completion(true)
            }
        } else {
            completion(false)
        }
    }
    
    func showAlert(withMessage message: String) {
        let alertVC = UIAlertController(title: "error", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertVC.addAction(action)
        present(alertVC, animated: true, completion: nil)
    }
}


