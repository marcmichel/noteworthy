//
//  LockStatus.swift
//  NoteWorthy
//
//  Created by Marc Michel on 10/2/18.
//  Copyright © 2018 Marc Michel. All rights reserved.
//

import Foundation

enum LockStatus {
    case locked
    case unlocked
}
